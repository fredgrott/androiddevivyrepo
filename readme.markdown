AndroidDevIvyRepo
================

AndroidDevIvyRepo, just a basic Ivy Repo for android development. Basically, we
are making use of the Ivy File resolver as it only increases the steps by two but
we still get the full power of being able to  manage project libs dependencies.

Project Lead
==========

Fred Grott is the project lead with some years of experience in both mobile,
enterprise, and LAMP MVC frameworks. His main blog sites are:

[Fred Grott-Posterous](http://fredgrott.posterous.com)
[Shareme-GithubPages](http://shareme.github.com)

Project License
============

Apache License 2.0

Implementation
=============
Our module naming convention needs to reflect the component and the project type,
however we do have one common type of component across most of the projects:

Notes on setup of the ivy scheme:

[Ivy schema](http://stackoverflow.com/questions/9204324/ivy-repository-structure-for-module-with-multiple-builds-profiles)
